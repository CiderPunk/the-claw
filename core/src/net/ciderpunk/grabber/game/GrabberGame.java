package net.ciderpunk.grabber.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import net.ciderpunk.gamebase.game.GameManager;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.gui.*;

/**
 * Created by Matthew on 18/04/2015.
 */
public class GrabberGame extends GameManager {

  Skin skin;
  Table baseTable;
  GrabberScreen screen;
  GameOverDialogue gameOver;
  LevelSelect levelSelect;
  FPSLogger logger;

  public GrabberGame() {
    super(new ExtendViewport(640,480));
  }

  @Override
  public void loadComplete() {
    Gdx.input.setInputProcessor(stage);
    baseTable = new Table();
    baseTable.setFillParent(true);

    stage.addActor(screen);
    stage.addActor(baseTable);

    ScoreDisplay score= new ScoreDisplay(screen, skin);
    HealthBar health = new HealthBar(screen, skin);

    gameOver = new GameOverDialogue(skin, screen);
    baseTable.add(score).expand().center().top().pad(20).row();
    baseTable.add(health).expand().bottom().left().pad(20);
    //baseTable.debugAll();
    //start game
    screen.init();
    stage.setKeyboardFocus(screen);
    logger = new FPSLogger();

    levelSelect = new LevelSelect(skin);
    levelSelect.show(stage);

  }

  @Override
  public void draw(float dt) {
    Gdx.graphics.getGL20().glClearColor(0.2f, 0.2f, 0.3f, 0);
    //Gdx.graphics.getGL20().glClearColor(1f, 1f, 1f, 0);
    Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    super.draw(dt);
    logger.log();
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.getAssetMan().load("res.json", Skin.class);
    resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
    screen = new GrabberScreen(this);
    resMan.addResourceUser(screen);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    skin = resMan.getAssetMan().get("res.json", Skin.class);
  }

  @Override
  public void resize(int width, int height) {
    super.resize(width, height);
    screen.updateSize(stage);
  }

  public void gameOver(){
    gameOver.show(stage);

  }

}
