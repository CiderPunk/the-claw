package net.ciderpunk.grabber;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.ciderpunk.grabber.game.GrabberGame;

public class Grabber extends ApplicationAdapter {

	GrabberGame game;

	@Override
	public void create () {
		game = new GrabberGame();
	}

	@Override
	public void render () {
		game.update();
	}

	@Override
	public void resize(int width, int height) {
		game.resize(width, height);
	}

	@Override
	public void dispose() {
		game.dispose();
		super.dispose();
	}
}
