package net.ciderpunk.grabber.ents;

/**
 * Created by Matthew on 19/04/2015.
 */
public interface IKillable {
  public boolean hurt(int damage);
  void Die();
}
