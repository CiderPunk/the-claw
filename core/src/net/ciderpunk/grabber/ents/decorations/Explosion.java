package net.ciderpunk.grabber.ents.decorations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.AnimatedEntity;
import net.ciderpunk.gamebase.ents.Entity2D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;

/**
 * Created by Matthew on 19/04/2015.
 */
public class Explosion extends AnimatedEntity implements Pool.Poolable, IResourceUser {

  private static final Pool<Explosion> pool = new Pool<Explosion>(){
    @Override
    protected Explosion newObject() {
      return new Explosion(null);
    }
  };

  static Animation explodeAnim;
  float scale, rot;
  static Vector2 temp = new Vector2();

  public Explosion() {
    super();
  }

  public Explosion(IEntityController owner) {
    super(owner);
  }



  public static Explosion GetExplosion(IEntityController owner, Vector3 position, float scale){
    return pool.obtain().init(owner,temp.set(position.x, position.y),scale);
  }

  public static Explosion GetExplosion(IEntityController owner, Vector2 position, float scale){
    return pool.obtain().init(owner,position,scale);
  }

  protected Explosion init(IEntityController owner, Vector2 position, float scale){
    setAnim(explodeAnim);
    this.loc.set(position);
    this.rot = MathUtils.random(360);
    this.scale = 0.5f * scale;
    return this;
  }

  @Override
  public boolean update(float dT) {
    if (!super.update(dT)){
      return false;
    }
    return true;
  }


  @Override
  public void cleanUp(){
    pool.free(this);
  }


  @Override
  protected boolean isLooping() {
    return false;
  }

  @Override
  public void draw(SpriteBatch batch) {
    currentFrame.draw(batch, loc.x, loc.y, rot, scale);
  }

  @Override
  public void preLoad(ResourceManager resMan) {
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    explodeAnim = buildAnim(resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class), "explosion/explode", 0.02f, 25, 25);
  }
  @Override
  public void reset() {

  }
}
