package net.ciderpunk.grabber.ents.decorations;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 19/04/2015.
 */
public class Column extends Entity3D implements Pool.Poolable, IResourceUser {

  static Model model;
  private float speed;


  private static final Pool<Column>  pool = new Pool<Column>() {
    @Override
    protected Column newObject() {
      return new Column(model);
    }
  };




  public Column init(IEntityController owner, Vector3 start, float speed) {
    this.owner = owner;
    this.loc.set(start);
    this.speed = speed;

    return this;
  }


  protected Pool getPool() {
    return pool;
  }


  public Column(Model model) {
    super(null);
    modelInstance = new ModelInstance(model, "architecture");
  }

  public Column() {
    super();
  }

  public Column(IEntityController owner) {
    super(owner);
  }

  public static Column GetColumn(IEntityController owner, Vector3 start, float speed){
    return pool.obtain().init(owner, start, speed);
  }

  @Override
  public void cleanUp() {
    getPool().free(this);
  }

  @Override
  public boolean update(float dT) {
    this.loc.mulAdd(Vector3.X, dT * speed);
    //return true;
    return getGame().testBounds(this.loc);
  }

  @Override
  public void preLoad(ResourceManager resMan) {

  }

  @Override
  public void draw(ModelBatch batch, Environment env) {


    modelInstance.transform.set(loc, qrt);
    modelInstance.transform.scl(20f);
    batch.render(modelInstance,  env);

  }

  @Override
  public void postLoad(ResourceManager resMan)
  {
    model = resMan.getAssetMan().get(Constants.ModelPath, Model.class);
  }

  protected GrabberScreen getGame(){return (GrabberScreen)owner;}

  @Override
  public void reset() {
  }
}
