package net.ciderpunk.grabber.ents.blufor;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.ents.IKillable;
import net.ciderpunk.grabber.ents.Marker;
import net.ciderpunk.grabber.ents.decorations.Explosion;
import net.ciderpunk.grabber.ents.opfor.OpFor;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Player extends Entity3D implements IResourceUser, IKillable {
  static Model model;
  ModelInstance clawMdl;
  boolean clawDocked;
  public Marker marker;
  Vector3 moveTarget = new Vector3();
  float tilt = 0f;
  float totalTime = 0f;
  static Vector3 diff = new Vector3();
  static Vector3 vel = new Vector3();
  static Vector3 target = new Vector3();

  OpFor captive;
  int health;
  boolean dead;

  private static final float MaxSpeed = 160f;
  private static final float TurnSpeed =60f;
  private static final float MaxPitch = 40f;
  private static final int StartHealth = 100;
  private static final int MaxHealth = 100;
  private static final int GameOverTTL = 3;
float ttl;

  Claw claw ;


  float timeOut;

  public Player() {
    super();
  }

  public Player(IEntityController owner) {
    super(owner);
    claw = new Claw(owner, this, model);
    marker = new Marker(owner);
    ((GrabberScreen)owner).addMissile(claw);
    ((GrabberScreen)owner).debugEnts.add(marker);
    modelInstance = new ModelInstance(model, "ship");
    clawMdl = new ModelInstance(model, "clawDocked");
    clawDocked = true;
    reset();
  }



  public void reset(){
    this.loc.set(-100f, 0f, 0f);
    health = StartHealth;
    resetMove();
    moveTarget.set(-50f,0,0);
    dead = false;
    clawDocked = true;
    captive = null;
    getGame().addMissile(claw);
  }


  @Override
  public void draw(ModelBatch batch, Environment env) {
    super.draw(batch, env);
    if (clawDocked) {
      clawMdl.transform.set(modelInstance.transform.getValues());
      batch.render(clawMdl, env);
    }
  }

  @Override
  public boolean update(float dT) {
    //get difference between current loc and target loc
    if(dead) {
      ttl-= dT;
      if (ttl <0){
        this.getGame().gameOver();

      }
      moveTarget.set(loc.x,loc.y - 100,0);
      //spawn random explosions
      if (MathUtils.random(6) == 0){
        this.getGame().addDecoration(Explosion.GetExplosion(owner, loc,MathUtils.random(0.5f, 2f)));
      }

    }


    totalTime+=dT;
    diff.set(moveTarget).sub(loc);
    vel.set(diff).nor().scl(dT * MaxSpeed);
    if (vel.len2() > diff.len2()) {
      loc.add(diff);
    }
    else {
      loc.add(vel);
    }
    float desiredTilt = MathUtils.clamp(diff.y * (-10f),-MaxPitch, MaxPitch);
    float diff = desiredTilt - tilt;
    tilt += MathUtils.clamp(diff, -dT * TurnSpeed, dT *  TurnSpeed);

    float sway = (dead ? dT * TurnSpeed : MathUtils.sin(totalTime) * 10f);


    this.setRotation(90, tilt, sway );


    //update captive position
    if (captive!= null) {
      captive.setRotation(this.qrt);
      captive.setLoc(qrt.transform(target.set(0,-2f,9f)).add(loc));
    }

    return true;
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.getAssetMan().load("models/grabber.g3db", Model.class);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    model = resMan.getAssetMan().get("models/grabber.g3db", Model.class);
  }

  public void updateTarget(float dX, float dY) {
    if (!dead) {
      moveTarget.x += dX * 0.4f;
      moveTarget.y += dY * 0.4f;
      moveTarget.x = MathUtils.clamp(moveTarget.x, -70, 40);
      moveTarget.y = MathUtils.clamp(moveTarget.y, -50, 50);
      marker.setPositon(moveTarget.x, moveTarget.y);
    }
  }

  public void resetMove() {
    moveTarget.set(loc);
    marker.setPositon(moveTarget.x, moveTarget.y);
  }

  public void launchClaw() {
    if (clawDocked && !dead){
      if (captive != null){
        //destory captive
        this.health += captive.getHealth();
        this.health = MathUtils.clamp(this.health, 0, MaxHealth);
        captive.kill();
      }
      else {
        //launch claw!!!
        clawDocked = false;
        claw.launch(target.set(loc.x + 100f, loc.y, 0));
      }
    }
  }

  public void dock(OpFor captureTarget) {
    this.clawDocked = true;
    //attach bad guy etc...
    if (captureTarget != null){
      captive = captureTarget;
      captive.coOpt(this);

      //switch captive's side!
      getGame().getOpFor().remove(captive);
      getGame().getBluFor().add(captive);
    }

  }

  @Override
  public boolean hurt(int damage) {
    this.health -= damage;
    if (health <= 0) {
      Kill();
    }
    return false;
  }

  public void Kill(){
    Die();
    dead = true;
    ttl = GameOverTTL;
  }

  @Override
  public void Die() {
    this.getGame().addDecoration(Explosion.GetExplosion(owner, loc, 1f));

  }

  @Override
  protected float getCollisionRadius() {
    return 5f;
  }
  protected GrabberScreen getGame(){return (GrabberScreen)owner;}

  public void captiveDead() {
    captive = null;
  }

  public float getHealth(){return (float) health / (float) MaxHealth; }
}
