package net.ciderpunk.grabber.ents.blufor;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.grabber.ents.opfor.OpFor;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Claw extends Entity3D {

  public void reset() {
    state= ClawState.Outbound;

  }

  public enum ClawState {
    Outbound,
    Inbound,
  }



  public static final float ClawSpeed = 160f;
  public static final float PitchSpeed = 600f;
  public static final float RollSpeed = 600f;


  Player player;
  Vector3 target = new Vector3();
  static Vector3 diff = new Vector3();
  static Vector2 vectToTarget = new Vector2();
  float roll;
  float pitch;
  boolean outbound;
  ClawState state;

  OpFor captureTarget;

  public Claw() {
    super();
  }

  public Claw(IEntityController owner, Player player, Model model) {
    super(owner);
    this.player = player;
    modelInstance = new ModelInstance(model, "claw");
    captureTarget = null;
    state = ClawState.Outbound;
  }


  @Override
  public void draw(ModelBatch batch, Environment env) {
    if (!player.clawDocked) {
      super.draw(batch, env);
    }
  }

  @Override
  public boolean update(float dT) {
    if (!player.clawDocked) {

      if (captureTarget == null) {
        Entity target = ((GrabberScreen) owner).getOpFor().pointCollisonTest(this.loc, 2f);
        if (target instanceof OpFor) {
          captureTarget = (OpFor) target;
          captureTarget.capture();
          state = ClawState.Inbound;
        }
      }
      switch (state){
        case Outbound:
         diff.set(target).sub(loc);
          if (diff.len2() > ( dT * dT * (ClawSpeed * ClawSpeed))) {
            loc.add(diff.nor().scl(dT * ClawSpeed));
          } else {
            state = ClawState.Inbound;
          }
          break;
        case Inbound:
          //return home!
          player.getLoc(diff).sub(loc);
          if (diff.len2() > (  dT * dT * (ClawSpeed * ClawSpeed))) {
            loc.add(diff.nor().scl(dT * ClawSpeed));
          }
          else {
            player.dock(captureTarget);
            captureTarget = null;
          }
          break;
      }

      roll += RollSpeed * dT;
      this.setRotation(90, pitch, roll);

      if (captureTarget != null) {
        diff.set(Vector3.Z).scl(6f);
        this.qrt.transform(diff).add(loc);
        captureTarget.setLoc(diff);
        captureTarget.setRotation(this.qrt);
      }
    }
    return true;
  }


  protected float turnDist(float val){
    while(val > 360){ val -= 360;}
    return val;
  }

  public void launch(Vector3 target) {
    this.target.set(target);
    player.getLoc(this.loc);
    player.getRotation(this.qrt);
    pitch = 0;// this.qrt.getPitch();
    roll = this.qrt.getRoll();
    state = ClawState.Outbound;

  }
}
