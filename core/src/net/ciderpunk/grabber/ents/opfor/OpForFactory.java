package net.ciderpunk.grabber.ents.opfor;

import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by matthewlander on 10/05/15.
 */
public class OpForFactory {

	private OpForFactory(){
		registry=new ObjectMap<String,OpForPool>();
	}

	protected final ObjectMap<String, OpForPool> registry;

	public final static OpForFactory INSTANCE = new OpForFactory();

	public void registerOpFor(String name, OpForPool pool){
		registry.put(name, pool);
	}

	public OpFor getOpFor(String type) throws Exception{
		OpForPool pool = registry.get(type);
		if (pool!= null){
			return (OpFor) pool.obtain();
		}
		throw new Exception(type + " not registered");
	}
}
