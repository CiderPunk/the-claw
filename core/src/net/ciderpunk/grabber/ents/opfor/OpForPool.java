package net.ciderpunk.grabber.ents.opfor;

import com.badlogic.gdx.utils.Pool;

/**
 * Created by matthewlander on 10/05/15.
 */
public abstract class OpForPool<T extends OpFor> extends Pool<T> {
}
