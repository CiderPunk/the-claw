package net.ciderpunk.grabber.ents.opfor;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.Entity3D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.grabber.config.opfor.OpForConfig;
import net.ciderpunk.grabber.config.paths.Path;
import net.ciderpunk.grabber.ents.IKillable;
import net.ciderpunk.grabber.ents.blufor.Player;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 18/04/2015.
 */
public abstract class OpFor extends Entity3D implements Pool.Poolable, IKillable {

  float yaw,roll,pitch;
  boolean dead;
  Player captor;
  int health;
  int score;


  protected OpForConfig config;
  //static PathCollection _paths;
  //protected abstract OpForConfig getConfig();

  public void capture() {
    this.state = OpForState.InTransit;
  }

  public void coOpt(Player plyr){
    this.state = OpForState.CoOpted;
    captor = plyr;
  }


  int getScoreValue() {
    return config.score;
  }

  public void kill() {
    getGame().addScore(this.getScoreValue());
    dead = true;
  }

  public int getHealth() {
    return health;
  }

  public enum OpForState{
    Free, //footlose fancy free as a bird shooting at the player
    CoOpted, // attached to player and being used as a shield
    InTransit, //hooked and reeling in
  }

  OpForState state;

  public OpFor() {
    super();
  }

  public OpFor(Model model) {
    super(null);
    state = OpForState.Free;
  }

  @Override
  public boolean update(float dT) {
    if (dead){
      //alert captor
      if (state == OpForState.CoOpted && captor != null) {
        captor.captiveDead();
      }
      Die();
      return false;
    }
    switch(state){
      case Free:
        if (!testBounds()){
          return false;
        }

        return updateFree(dT);
      case CoOpted:
        return updateCaptured(dT);
      case InTransit:
        return updateTransit(dT);
    }
    return true;
  }

  //checks for collision with player owned stuff
  protected boolean testCollision(){
    Entity target = getGame().getBluFor().pointCollisonTest(this.loc, this.getCollisionRadius());
    if (target != null){
      if (target instanceof IKillable) {
        return collide(target);
      }
    }
    return true;
  }


  protected boolean collide(Entity target) {
    ((IKillable) target).hurt(this.health * 2);
    this.Die();
    return false;
  }


  protected boolean updateTransit(float dT) {
    return true;
  }

  protected abstract boolean updateCaptured(float dT);

  protected abstract boolean updateFree(float dT);

  @Override
  public void reset() {
    state = OpForState.Free;
    dead = false;
    captor = null;
  }

  protected boolean testBounds(){
    return ((GrabberScreen)owner).testBounds(this.loc);
  }

  protected GrabberScreen getGame(){return (GrabberScreen)owner;}

  protected abstract OpForPool getPool();

  @Override
  public void cleanUp(){
    getPool().free(this);
  }

  @Override
  public boolean hurt(int damage) {
    switch(state){
      case Free:
      case CoOpted:
        this.health -= damage;
        if (health <= 0) {

          //HAHAH!
          kill();
        }
        break;
      case InTransit:
        break;
    }
    //blow up or some shit..
    return true;
  }


  public abstract void startAttackRun(Vector3 start, Vector3 target);


  public OpFor init(IEntityController owner, OpForConfig conf) {
    this.owner = owner;
    health = conf.health;
    this.config  = conf;
    return this;
  }



}