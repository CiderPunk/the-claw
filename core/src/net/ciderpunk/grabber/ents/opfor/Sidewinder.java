package net.ciderpunk.grabber.ents.opfor;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.opfor.OpForConfig;
import net.ciderpunk.grabber.config.paths.Path;
import net.ciderpunk.grabber.ents.decorations.Explosion;
import net.ciderpunk.grabber.ents.missile.Bullet;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Sidewinder extends OpFor implements Pool.Poolable, IResourceUser {


  protected static final float MaxSpeed = 120f;
  protected static final float SpeedVariance = 40f;

  protected static final float FireInit = 0.2f;
  protected static final float FirePeriod = 0.8f;
  protected static final float FireVariance = 0.2f;

  protected static final float CaptiveFirePeriod = 0.4f;

  protected static final float BulletSpeed = 200f;
  protected static final float BulletScale = 0.5f;
//  protected static final int BulletDamage = 10;

/*
  protected static final int StartHealth = 10;
  protected static final int ScoreWorth = 100;
*/
  Vector3 velocity = new Vector3();
  static Vector3 temp = new Vector3();
  static Vector2 bulletVel = new Vector2();

  private static final OpForPool<Sidewinder>  pool = new OpForPool<Sidewinder>() {
    @Override
    protected Sidewinder newObject() {
      return new Sidewinder(model);
    }
  };
  static Model model;

  static{
    OpForFactory.INSTANCE.registerOpFor("sidewinder", pool);
  }

  float roll;
  float rollRate;
  float fireTime;

  //private static OpForConfig config;
  private static Path[] paths;

  public Sidewinder() {
    super();
  }

  @Override
  protected boolean updateCaptured(float dT) {
    fireTime -= dT;
    if (fireTime < 0f) {

      temp.set(Vector3.Z).scl(BulletSpeed);
      this.qrt.transform(temp);

      Bullet bullet = Bullet.getBullet(owner, ((GrabberScreen) owner).getOpFor(), this.loc, bulletVel.set(temp.x, temp.y), BulletScale, config.bulletDamage);
      getGame().addMissile(bullet);
      fireTime = CaptiveFirePeriod;
    }
    return true;
  }

  public Sidewinder(Model model) {
    super(null);
    modelInstance = new ModelInstance(model, "sidewinder");
  }

  public static Sidewinder GetSidewinder(IEntityController owner, OpForConfig config){
    return (Sidewinder)pool.obtain().init(owner, config);
  }

  @Override
  protected boolean updateFree(float dT) {
    this.loc.mulAdd(this.velocity, dT);
    roll += dT * rollRate;
    qrt.setEulerAngles(-90, 0, roll);
    fireTime -= dT;
    if (fireTime < 0f) {
      Bullet bullet = Bullet.getBullet(owner,((GrabberScreen) owner).getBluFor(),this.loc,bulletVel.set(Vector2.X).scl( -BulletSpeed), BulletScale, config.bulletDamage);
      getGame().addMissile(bullet);
      //next shoot.
      fireTime = FirePeriod - MathUtils.random(FireVariance);
    }
    return testCollision();
  }

  @Override
  protected OpForPool getPool() {
    return pool;
  }



  public void startAttackRun(Vector3 start, Vector3 target){
    this.loc.set(start);
    velocity.set(target).sub(start).nor().scl(MaxSpeed - MathUtils.random(SpeedVariance));
    roll = MathUtils.random(360);
    //rollRate = MathUtils.random(-300f, 300f);
    rollRate = 200f - MathUtils.random(40f) * (MathUtils.random(0,1) == 1 ? -1: 1);
    qrt.setEulerAngles(-90,0,roll);
    fireTime = FireInit - MathUtils.random(FireVariance);
  }

  @Override
  public void preLoad(ResourceManager resMan) {
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    model = resMan.getAssetMan().get(Constants.ModelPath, Model.class);
  }

  @Override
  public void Die() {
    this.getGame().addDecoration(Explosion.GetExplosion(owner, loc, 1f));
  }

  @Override
  protected float getCollisionRadius() {
    return 8f;
  }
}
