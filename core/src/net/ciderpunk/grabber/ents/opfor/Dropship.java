package net.ciderpunk.grabber.ents.opfor;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.opfor.OpForConfig;
import net.ciderpunk.grabber.config.paths.Path;
import net.ciderpunk.grabber.ents.blufor.Player;
import net.ciderpunk.grabber.ents.decorations.Explosion;
import net.ciderpunk.grabber.ents.missile.Bullet;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Dropship extends OpFor implements Pool.Poolable, IResourceUser {

  protected static final float MaxSpeed = 80f;
  protected static final float SpeedVariance = 20f;

  protected static final float FireInit = 0.4f;
  protected static final float FirePeriod = 1.2f;
  protected static final float FireVariance = 0.2f;

  protected static final float CaptiveFirePeriod = 1f;

  protected static final float BulletSpeed = 200f;
  protected static final float BulletScale = 0.5f;
  protected static final int BulletCount = 3;

  protected static final float BurstFirePeriod = 0.05f;

  int fireCount;
/*
  protected static final int StartHealth = 25;
  protected static final int ScoreWorth = 250;
*/
  Vector3 velocity = new Vector3();
  static Vector3 temp = new Vector3();
  static Vector2 bulletVel = new Vector2();

  private static final OpForPool<Dropship>  pool = new OpForPool<Dropship>() {
    @Override
    protected Dropship newObject() {
      return new Dropship(model);
    }
  };

  static{
    OpForFactory.INSTANCE.registerOpFor("dropship", pool);
  }


  static Model model;

  float roll;
  float rollRate;
  float fireTime;
  float totalTime;

  //private static OpForConfig config;
  //private  static Path[] paths;

  /*
  @Override
  protected OpForConfig getConfig() {
    return config== null ? config = GameConfig.getGameConfig().opfor.getConfig("dropship") : config;
  }
*/
  @Override
  public void coOpt(Player plyr) {
    super.coOpt(plyr);
    fireCount = BulletCount;
    fireTime = CaptiveFirePeriod;
  }

  public Dropship() {
    super();
  }

  @Override
  protected boolean updateCaptured(float dT) {
    fireTime -= dT;
    if (fireTime < 0f) {
      if (fireCount > 0){
        fireCount--;
        fireTime = BurstFirePeriod;
      }
      else{
        fireCount = BulletCount;
        fireTime = CaptiveFirePeriod;
      }
      temp.set(Vector3.Z);
      this.qrt.transform(temp);
      Bullet bullet = Bullet.getBullet(owner, ((GrabberScreen) owner).getOpFor(), this.loc, bulletVel.set(temp.x, temp.y + MathUtils.random(-0.1f, 0.1f)).nor().scl(BulletSpeed), BulletScale, config.bulletDamage);
      getGame().addMissile(bullet);


    }
    return true;
  }

  public Dropship(Model model) {
    super(null);
    modelInstance = new ModelInstance(model, "dropship");
  }

  public static Dropship GetDropship(IEntityController owner, OpForConfig config){
    return (Dropship)pool.obtain().init(owner,config);
  }

  @Override
  protected boolean updateFree(float dT) {
    this.loc.mulAdd(this.velocity, dT);
    totalTime+= dT;
    roll =  MathUtils.sin(totalTime) * 20f;
    qrt.setEulerAngles(-90, 0, roll);

    fireTime -= dT;
    if (fireTime < 0f) {
      if (fireCount > 0) {
        fireCount--;
        fireTime = BurstFirePeriod;
      } else {
        fireCount = BulletCount;
        fireTime = FirePeriod;
      }
      temp.set(Vector3.Z);
      this.qrt.transform(temp);
      Bullet bullet = Bullet.getBullet(owner, ((GrabberScreen) owner).getBluFor(), this.loc, bulletVel.set(1f, MathUtils.random(-0.1f, 0.1f)).nor().scl(-BulletSpeed), BulletScale, config.bulletDamage);
      getGame().addMissile(bullet);
    }

    return testCollision();
  }

  @Override
  protected OpForPool getPool() {
    return pool;
  }
/*
  public Dropship init(IEntityController owner) {
    this.owner = owner;
    health = getConfig().health;
    fireCount = BulletCount;
    return this;
  }
*/
  public void startAttackRun(Vector3 start, Vector3 target){
    this.loc.set(start);
    velocity.set(target).sub(start).nor().scl(MaxSpeed - MathUtils.random(SpeedVariance));
    //roll = MathUtils.random(360);
    //rollRate = MathUtils.random(-300f, 300f);
    rollRate = 200f - MathUtils.random(40f) * (MathUtils.random(0,1) == 1 ? -1: 1);
    qrt.setEulerAngles(-90,0,roll);
    fireTime = FireInit - MathUtils.random(FireVariance);
  }

  @Override
  public void preLoad(ResourceManager resMan) {
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    model = resMan.getAssetMan().get(Constants.ModelPath, Model.class);
  }


  @Override
  public void Die() {
    this.getGame().addDecoration(Explosion.GetExplosion(owner, loc, 1f));
  }

  @Override
  protected float getCollisionRadius() {
    return 8f;
  }
}
