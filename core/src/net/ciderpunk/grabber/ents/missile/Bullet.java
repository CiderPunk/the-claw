package net.ciderpunk.grabber.ents.missile;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.Entity2D;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.ents.IKillable;
import net.ciderpunk.grabber.ents.decorations.Explosion;
import net.ciderpunk.grabber.gui.GrabberScreen;

/**
 * Created by Matthew on 19/04/2015.
 */
public class Bullet extends Entity2D implements IResourceUser, Pool.Poolable {

  static Frame bulletFrame;
  static Vector2 temp = new Vector2();
  Vector2 vel = new Vector2();
  EntityList targets;
  int damage;
  float scale;

  protected static Pool<Bullet> pool = new Pool<Bullet>() {
    @Override
    protected Bullet newObject() {
      return new Bullet(null);
    }
  };

  public Bullet(IEntityController owner) {
    super(owner);
  }

  public Bullet() {
    super();
  }

  @Override
  public void draw(SpriteBatch batch) {
    currentFrame.draw(batch, loc.x, loc.y, 0f, scale);
  }

  public static Bullet getBullet(IEntityController owner, EntityList targets, Vector2 start, Vector2 velocity, float scale, int damage){
    return pool.obtain().init(owner, targets, start, velocity, scale, damage);
  }

  public static Bullet getBullet(IEntityController owner, EntityList targets, Vector3 start, Vector2 velocity, float scale, int damage){
    return getBullet(owner, targets, temp.set(start.x, start.y), velocity, scale, damage);
  }

  protected Bullet init(IEntityController owner, EntityList targets, Vector2 start, Vector2 velocity, float scale, int damage){
    this.currentFrame = bulletFrame;
    this.owner = owner;
    this.loc.set(start);
    this.vel.set(velocity);
    this.targets = targets;
    this.scale = scale;
    this.damage = damage;
    return this;
  }

  @Override
  public boolean update(float dT) {
    this.loc.mulAdd(vel, dT);
    Entity target = targets.pointCollisonTest(this.getLoc(), 4f * scale);
    if (target != null){
      if (target instanceof IKillable) {
        ((IKillable) target).hurt(this.damage);
        Explosion  exp = Explosion.GetExplosion(owner, this.loc, scale);
        ((GrabberScreen)owner).addDecoration(exp);
      }
      return false;
    }
    return true;
  }

  @Override
  public void preLoad(ResourceManager resMan) {
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    bulletFrame = new Frame(resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class).findRegion("bullet"),5,5);
  }

  @Override
  public void reset() {

  }

  @Override
  public void cleanUp(){
    pool.free(this);
  }


}
