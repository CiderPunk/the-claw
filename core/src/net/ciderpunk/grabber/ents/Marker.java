package net.ciderpunk.grabber.ents;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import net.ciderpunk.gamebase.ents.Entity2D;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Marker extends Entity2D implements IResourceUser {
  static Frame pointer;

  public Marker() {
    super();
  }

  public Marker(IEntityController owner) {
    super(owner);
    currentFrame = pointer;
  }

  public void setPositon(float x, float y){
    this.loc.set(x,y);
  }

  @Override
  public boolean update(float dT) {
    return true;
  }

  @Override
  public void preLoad(ResourceManager resMan) {

  }

  @Override
  public void postLoad(ResourceManager resMan) {
    pointer = new Frame(resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class).findRegion("pointer"),5,5);
  }
}
