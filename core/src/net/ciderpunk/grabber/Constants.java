package net.ciderpunk.grabber;

/**
 * Created by Matthew on 18/04/2015.
 */
public class Constants {

  public static final String AtlasPath = "res.atlas";
  public static final String SkinPath = "res.json";
  public static final String ModelPath = "models/grabber.g3db";
  public static final float CameraHeight = 80f;
  public static final float StartSpawnRate = 5000f;
  public static final float spawnIncrease = 6f;
  public static final String ConfigPath = "config/config.json";
}
