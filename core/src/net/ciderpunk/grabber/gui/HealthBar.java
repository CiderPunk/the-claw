package net.ciderpunk.grabber.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.ents.blufor.Player;

/**
 * Created by Matthew on 19/04/2015.
 */
public class HealthBar extends Actor {

  final GrabberScreen screen;
  final Skin skin;
  NinePatchDrawable frame;
  NinePatchDrawable inner;

  public HealthBar(GrabberScreen screen, Skin skin){
    super();
    this.screen = screen;
    this.skin = skin;

    this.setWidth(200);
    this.setHeight(30);
    this.setTouchable(Touchable.disabled);
    Color tint = skin.getColor("fade");
    frame = new NinePatchDrawable(skin.getPatch("gui/healthframe")).tint(tint);
    inner = new NinePatchDrawable(skin.getPatch("gui/healthbar")).tint(tint);

  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    Color col = getColor();
    batch.setColor(col.r, col.g, col.b, col.a * parentAlpha);
    if (screen.isActive() && screen.getPlayer() != null) {
      frame.draw(batch, getX(), getY(), getWidth(), getHeight());
      if (screen.getPlayer().getHealth() > 0) {
        inner.draw(batch, getX() + 3, getY() + 3,screen.getPlayer().getHealth()  * (getWidth() - 6), getHeight() - 6);
      }
    }
    batch.setColor(col.r, col.g, col.b, 1f);
  }
}
