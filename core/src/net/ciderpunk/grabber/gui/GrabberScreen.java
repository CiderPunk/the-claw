package net.ciderpunk.grabber.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.gamebase.ents.IEntityController;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.gamebase.utils.NumberFormatter;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.ents.Marker;
import net.ciderpunk.grabber.ents.blufor.Player;
import net.ciderpunk.grabber.ents.decorations.Column;
import net.ciderpunk.grabber.ents.decorations.Explosion;
import net.ciderpunk.grabber.ents.missile.Bullet;
import net.ciderpunk.grabber.ents.opfor.Dropship;
import net.ciderpunk.grabber.ents.opfor.OpFor;
import net.ciderpunk.grabber.ents.opfor.Sidewinder;
import net.ciderpunk.grabber.game.GrabberGame;

/**
 * Created by Matthew on 18/04/2015.
 */
public class GrabberScreen extends Actor implements Disposable, IResourceUser, IEntityController {

  boolean active;
  Camera cam;
  SpriteBatch spriteBatch;
  ModelBatch modelBatch;
  Environment environment;
  //EntityList ents;
  EntityList opFor;
  EntityList bluFor;
  EntityList missiles;
  EntityList decorations;
  GrabberScreen self;
  Player player;

  GameConfig gameConfig;

  public EntityList debugEnts;
  static Vector3 tempVect3 = new Vector3();
  static Vector2 tempVect = new Vector2();
  static Vector3 startLoc = new Vector3(), temploc = new Vector3();
  Rectangle bounds = new Rectangle();

  long lastScore = 0;
  long score;
  String formattedScore;
  final GrabberGame game;


  private final static float ColumnDistance = 4f;
  float colTime;
  static final Vector3 ColumnStart = new Vector3(160f,0f,-2f);
  private final static float ColumnSpeed = -30f;


  float spawnRate;


  @Override
  public Actor hit(float x, float y, boolean touchable) {
    return super.hit(x, y, touchable);
  }

  public GrabberScreen(GrabberGame game) {
    this.game = game;
    self = this;
    decorations = new EntityList();
    debugEnts = new EntityList();
    opFor = new EntityList();
    bluFor = new EntityList();
    missiles = new EntityList();

    modelBatch = new ModelBatch();
    spriteBatch = new SpriteBatch();
    cam = new PerspectiveCamera(75, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    cam.position.set(0f, 0f, Constants.CameraHeight);
    cam.lookAt(0f, 0f, 0f);
    environment = new Environment();
    environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.3f, 0.2f, 0.2f, 1.0f));
    environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));




    this.addListener(new ClickListener() {

      boolean moving;
      float lastX, lastY;


      @Override
      public boolean keyDown(InputEvent event, int keycode) {
        player.launchClaw();
        return true;
      }
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        //System.out.printf("touch %f %f %d %d\n", x, y, button, pointer);
        int i = pointer | button;


        if (i == 0 ) {
          moving = true;
          lastX = x;
          lastY = y;
          player.resetMove();
        } else  {
          player.launchClaw();
      }
        return true;
      }


      @Override
      public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (pointer == 0 && moving) {
          player.updateTarget(x - lastX, y - lastY);
          lastX = x;
          lastY = y;
          //System.out.printf("drag %f %f\n ", x, y);
          super.touchDragged(event, x, y, pointer);
        }
      }

      @Override
      public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        //System.out.printf("touch up %f %f %d %d\n", x, y, button, pointer);
        int i = pointer | button;
        if (i == 0) {
          player.resetMove();
          moving = false;
        }
      }
    });
  }


  @Override
  protected void setStage(Stage stage) {
    super.setStage(stage);
    updateSize(stage);
  }

  public void updateSize(Stage stage){
    if (stage!= null) {
      this.setWidth(stage.getWidth());
      this.setHeight(stage.getWidth());
      cam.update();

      //update bounds 'cos of ethics
      cam.unproject(tempVect3.set(0, Gdx.graphics.getHeight(), 1));
      bounds.x =  tempVect3.x ;
      bounds.y = tempVect3.y;
      cam.unproject(tempVect3.set(Gdx.graphics.getWidth(), 0,1));
      bounds.width =  tempVect3.x - bounds.x;
      bounds.height = tempVect3.y - bounds.y;

      //grow by 100 for some reason
      bounds.x+=-100f;
      bounds.y+=-100f;
      bounds.width+=500f;
      bounds.height+=200f;
    }
  }


  public void reset(){
    score = 0;
    lastScore = 100;
    opFor.clear();
    decorations.clear();
    missiles.clear();
    active = true;
    colTime = 0;
    player.reset();
    spawnRate = Constants.StartSpawnRate;
  }

  public void init(){
    player = new Player(this);
    bluFor.add(player);
    reset();
  }


  @Override
  public void act(float dT) {
    if (active) {
      if ((colTime -= dT) < 0) {
        colTime = ColumnDistance;
        decorations.add(Column.GetColumn(this, ColumnStart, ColumnSpeed));
      }


      /*
      spawnRate -= Constants.spawnIncrease * dT;

      if (MathUtils.random( (int)Math.floor(dT * spawnRate)) == 0){

        OpFor ent;
        switch (MathUtils.random(4)){
          case 0:
            ent = Dropship.GetDropship(this);
            break;
          default:
            ent = Sidewinder.GetSidewinder(this);
            break;
        }

        opFor.add(ent);
        ent.startAttackRun(startLoc.set(100, MathUtils.random(-70, 70), 0), player.getLoc(temploc));
      }
      */
      bluFor.doUpdate(dT);
      opFor.doUpdate(dT);
      missiles.doUpdate(dT);
      debugEnts.doUpdate(dT);
      decorations.doUpdate(dT);
      cam.position.set(0, player.getLoc(tempVect3).y / 10f, Constants.CameraHeight);

    }
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    batch.end();

    cam.update();
    //draw 2D stuff
    spriteBatch.setProjectionMatrix(cam.combined);
    spriteBatch.begin();
    missiles.doDraw(spriteBatch);
    spriteBatch.end();


    //draw columns / decs
    modelBatch.begin(cam);
    decorations.doDraw(modelBatch, environment);
    modelBatch.end();

    //draw decorations and bullets
    spriteBatch.begin();
    missiles.doDraw(spriteBatch);
    spriteBatch.end();

    //draw player and baddies
    modelBatch.begin(cam);
    bluFor.doDraw(modelBatch, environment);
    opFor.doDraw(modelBatch, environment);
    missiles.doDraw(modelBatch, environment);

    //explosions
    spriteBatch.begin();
    decorations.doDraw(spriteBatch);
    spriteBatch.end();

    modelBatch.end();
    batch.begin();
  }

  @Override
  public void dispose() {
    spriteBatch.dispose();
    modelBatch.dispose();
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.addResourceUser(new IResourceUser[]{new Player(), new Marker(), new Sidewinder(), new Dropship(), new Explosion(), new Bullet(), new Column()});
    //gameConfig = GameConfig.loadConfig(Constants.ConfigPath);
    //preload config
    GameConfig.getGameConfig();
  }

  @Override
  public void postLoad(ResourceManager resMan) {}

  public boolean testBounds(Vector3 vect){return testBounds(vect.x, vect.y);}
  public boolean testBounds(Vector2 vect){
    return testBounds(vect.x, vect.y);
  }
  public boolean testBounds(float x, float y){
    return bounds.contains(x,y);
  }

  public void addMissile(Entity missile){
    missiles.add(missile);
  }
  public void addDecoration(Entity dec){
    decorations.add(dec);
  }

  public EntityList getOpFor(){ return this.opFor;}
  public EntityList getBluFor(){ return this.bluFor;}

  public Player getPlayer(){return player;}
  public boolean isActive(){return active;}

  public String getScore() {
    if (score!= lastScore){
      formattedScore = NumberFormatter.formatNumber(score);
      lastScore = score;
    }
    return formattedScore;
  }

  public void addScore(int scoreValue) {
    score += scoreValue;
  }

  public void gameOver() {
    game.gameOver();
    active = false;
  }
}
