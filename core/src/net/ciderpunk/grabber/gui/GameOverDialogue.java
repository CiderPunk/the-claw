package net.ciderpunk.grabber.gui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Matthew on 19/04/2015.
 */
public class GameOverDialogue extends Dialog{

  final GameOverDialogue self;
  final GrabberScreen screen;
  Label contentLabel;

  public GameOverDialogue(Skin skin, GrabberScreen gameScteen) {

    super("Game Over", skin, "dialog");
    self = this;
    this.screen = gameScteen;
    this.pad(30, 10, 10, 10);
    contentLabel = new Label("", skin);
    this.getContentTable().add(contentLabel);

    TextButton btn = new TextButton("Restart", skin);
    btn.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        screen.reset();
        self.hide();
      }
    });
    this.getButtonTable().add(btn);
  }


  @Override
  public Dialog show(Stage stage) {
    String content = "Nice try!\nNext time, I suggest you avoid dying\n\nYour score was:" +screen.getScore() + "\n\nCreated by @CiderPunk\n19th April 2015\nfor Ludum Dare #32";
    contentLabel.setText(content);
    this.getContentTable().pack();
    this.pack();
    return super.show(stage);
  }
}
