package net.ciderpunk.grabber.gui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Created by Matthew on 19/04/2015.
 */
public class ScoreDisplay extends Label {

  final GrabberScreen screen;

  public ScoreDisplay(GrabberScreen screen, Skin skin) {
    super("", skin, "score-font", skin.getColor("score-colour"));
    this.screen = screen;
    this.setTouchable(Touchable.disabled);

  }

  @Override
  public void act(float delta) {
    this.setText(screen.getScore());
    super.act(delta);
  }
}
