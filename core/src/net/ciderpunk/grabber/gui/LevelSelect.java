package net.ciderpunk.grabber.gui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.level.Chapter;

import java.io.Console;

/**
 * Created by matthewlander on 30/05/15.
 */
public class LevelSelect extends Dialog {
	final LevelSelect self;

	final ScrollPane scroller;
	final List<Chapter> levelList;

	public LevelSelect( Skin skin) {
		super("Level select", skin);
		self = this;
		//	List
		levelList = new List<Chapter>(skin);


		levelList.setItems(GameConfig.getGameConfig().chapters.toArray(Chapter.class));
		levelList.getSelection().setMultiple(false);
		levelList.getSelection().setRequired(true);

		levelList.setSelectedIndex(0);
		scroller = new ScrollPane(levelList, skin);
		scroller.setSize(100f, 100f);

		this.pad(30, 10, 10, 10);
		this.getContentTable().add(scroller);

		TextButton goBtn = new TextButton("Go!", skin);
		goBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//screen.reset();
				System.out.printf("CHAPTER %s\n",levelList.getSelected().name);
				self.hide();
			}
		});
		this.getButtonTable().add(goBtn);

this.pack();
		//levelList.setItems(GameConfig.getGameConfig().chapters.toArray(Chapter.class));
	}
}
