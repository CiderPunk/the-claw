package net.ciderpunk.grabber.config.level;

import net.ciderpunk.grabber.config.NamedNode;

/**
 * Created by Matthew on 04/05/2015.
 * collection of stages
 */
public class Chapter extends NamedNode{
  Stage[] stages;
  public String displayName;

  @Override
  public String toString() {
    return displayName != null ? displayName : name;
  }
}
