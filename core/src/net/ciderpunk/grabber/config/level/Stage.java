package net.ciderpunk.grabber.config.level;

import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.section.Section;

/**
 * Created by matthewlander on 04/05/15.
 */
public class Stage {
  public String name;
	public String[] sections;

  public Section[] getSections() {
    Section[] sects = new Section[sections.length];
    for (int i = 0; i < sections.length; i++) {
      sects[i] = GameConfig.getGameConfig().sections.getConfig(sections[i]);
    }
    return sects;
  }
	//TODO: decortations / environment / flavour text etc.
}
