package net.ciderpunk.grabber.config;

import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

/**
 * Created by matthewlander on 30/04/15.
 */
public class ConfigCollection<T extends NamedNode> {

	public T getConfig(String name){
		Iterator<T> i = content.iterator();
		while(i.hasNext()){
			T next = i.next();
			if (next.name.equals(name)) {
				return next;
			}
		}
		return null;
	}

	protected Array<T> content;

	public T[] toArray(Class type){
		return (T[])content.toArray(type);
	}

	public ConfigCollection<T> merge(ConfigCollection<T> target){
		if (target != null) {
			content.ensureCapacity(target.content.size);
			Iterator<T> i = target.content.iterator();
			while (i.hasNext()) {
				T next = i.next();
				//see if we have an eleemnt with that name,,,,
				if (getConfig(next.name) == null) {
					//add
					content.add(next);
				}
			}
		}
		return this;
	}
}
