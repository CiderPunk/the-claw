package net.ciderpunk.grabber.config;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import net.ciderpunk.grabber.Constants;
import net.ciderpunk.grabber.config.level.Chapter;
import net.ciderpunk.grabber.config.opfor.OpForConfig;
import net.ciderpunk.grabber.config.paths.Path;
import net.ciderpunk.grabber.config.section.Section;

/**
 * Created by matthewlander on 26/04/15.
 */
public class GameConfig {

	protected static GameConfig loadConfig(String filePath){
		FileHandle file = Gdx.files.internal(filePath);
		Json json = new Json();
		json.addClassTag("opfor", OpForConfig.class);
    json.addClassTag("path", Path.class);
    json.addClassTag("chapter", Chapter.class);
    json.addClassTag("section", Section.class);
		return json.fromJson(GameConfig.class, file).loadDependants();
	}

	protected GameConfig loadDependants() {
		if (include != null) {
			for (int i = 0; i < include.length; i++) {
				GameConfig inc = loadConfig(include[i]);
				opfor = (opfor == null ? inc.opfor : opfor.merge(inc.opfor));
        paths = (paths == null ? inc.paths : paths.merge(inc.paths));
        chapters = (chapters == null ? inc.chapters : chapters.merge(inc.chapters));
        sections = (sections == null ? inc.sections : sections.merge(inc.sections));
			}
		}
		return this;
	}

	private static GameConfig gameConfig;

	public static synchronized GameConfig getGameConfig(){
		if (gameConfig == null){
			gameConfig = loadConfig(Constants.ConfigPath);
		}
		return gameConfig;
	}


	public ConfigCollection<OpForConfig> opfor;
	public ConfigCollection<Path> paths;
  public ConfigCollection<Chapter> chapters;
  public ConfigCollection<Section> sections;
	public String[] include;

}
