package net.ciderpunk.grabber.config.opfor;

import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.paths.Path;

/**
 * Created by matthewlander on 02/05/15.
 */
public class SpawnConfig {

	public int waveSize;
	public int waveVariance;
	public float vertDist;
	public float timeDist;

	public float max, min;
	public String[] paths;

	protected Path[] _paths;

	public Path[] getPaths(){
		if (_paths != null){
			_paths = new Path[_paths.length];
			for(int i = 0; i < _paths.length; i++) {
				_paths[i] = GameConfig.getGameConfig().paths.getConfig(paths[i]);
			}
		}
		return _paths;
	}
}
