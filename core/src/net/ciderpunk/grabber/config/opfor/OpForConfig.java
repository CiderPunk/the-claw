package net.ciderpunk.grabber.config.opfor;

import net.ciderpunk.grabber.config.ConfigCollection;
import net.ciderpunk.grabber.config.GameConfig;
import net.ciderpunk.grabber.config.NamedNode;
import net.ciderpunk.grabber.config.paths.Path;

/**
 * Created by matthewlander on 26/04/15.
 */
public class OpForConfig extends NamedNode {

	public String model;
	public String material;
	public float scale;
	public int score;
	public int health;
	public int bulletDamage;

	public SpawnConfig[] spawns;
	public OpForConfig() {
		super();
		scale = 1f;
		model = "sidewinder";
	}


}
