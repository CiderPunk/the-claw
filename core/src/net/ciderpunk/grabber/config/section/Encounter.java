package net.ciderpunk.grabber.config.section;

/**
 * Created by matthewlander on 04/05/15.
 * represents a set number of potential encounters in a sections time with the specified opfor on chance
 */
public class Encounter {
	public String opfor;
	public float chance;
	public int count;
}
