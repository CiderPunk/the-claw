package net.ciderpunk.grabber.config.section;

import net.ciderpunk.grabber.config.NamedNode;

/**
 * Created by matthewlander on 04/05/15.
 * section of a stage (level)
 */
public class Section extends NamedNode {
  //time for stage
	public float time;
  //list of encounters
	public Encounter[] enc;
}
