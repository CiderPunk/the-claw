package net.ciderpunk.threedee.packer;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import java.lang.Exception;

public class GrabberPacker {
	public static void main (String[] arg) throws Exception{
		TexturePacker.Settings settings = new TexturePacker.Settings();
		TexturePacker.process(settings, "atlas/", "android/assets", "res");
	}
}
